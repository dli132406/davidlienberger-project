'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    if (req.query.name) {
        res.send(`Guten Tag, ${req.query?.name}! Wie geht es dir heute?`)
    } else {
        res.send('Hallo Benutzer. Mit dem Parameter name erhältst du eine persönliche Begrüssungsnachricht.\nBeispiel: ?name=Tom');
    }
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});
